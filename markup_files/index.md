![Bild 1](media/1.jpeg)

**Genom ”Avtal för digitala leveranser 2010” kan man tydligt reglera vad som gäller för den digitala informationen och ha detta avtal som en bilaga till ABK09. Illustration: Bjerking AB**

# Viktigt att skriva avtal om digitala leveranser

> ##### Den digitala informationen i ett byggprojekt blir allt mer omfattande vilket ökar behovet av ett avtal som reglerar de legala aspekterna av denna information. Nu finns en enkel mall för avtal om digitala leveranser som skapar ordning och reda inom det här området. 

– DET ÄR HÖG TID ATT SKRIVA AVTAL FÖR DIGITALA leveranser. Vår avtalsmall reglerar framför allt leveransspecifikation, nyttjanderätt och ansvar – samtliga områden lika viktiga för helheten, säger Håkan Blom, HL Blom Konsult AB, som på uppdrag av OpenBIM lett arbetet med att arbeta fram avtalsmallen.
​	Normalt skriver uppdragsgivare och konsult avtal om uppdraget utifrån standardavtalet ABK som har funnits under många år och som skapades i en tid då de flesta handlingar i ett byggprojekt var pappersbaserade. Efterhand som datorarbetet allt mer tagit över som arbetsredskap har fler och fler datafiler bibringats projekthandlingarna. Ofta skickas filerna ut utan att det finns något avtalat om dessa filer.
​	Genom BIM kan man idag få en obruten informationskedja från tidig projektering till bygghandling till byggande där informationen så småningom hamnar hos beställaren som ska förvalta byggnaden.
​	– Beställarens intresse för att även datafilerna ska ingå i leveransen har därmed ökat, men detta område är i princip avtalslöst. Det finns inget i ABK09 som reglerar vem som äger databasen, vad upphovsrätten är värd och vad som gäller för
nyttjanderätt och ansvar för den digitala informationen, säger Håkan Blom och fortsätter:
​	– Genom vår avtalsmall kan man tydligt reglera vad som gäller för den digitala informationen och ha detta avtal som en bilaga till ABK09. Då undviker man att hamna i situationer där ansvarsfrågan och nyttjanderätten är oklar. Om man inte tecknar
avtal kan det dessutom bli svårt att i efterhand komma överens om priset för den digitala informationen.
​	Nya ABK09 överlåter åt parterna att avtala om hur uppdragsresultatet ska arkiveras, vilket i tidigare avtalsversioner av ABK ålades konsulten. Avtalsmallen ger utrymme för att precisera hur arkiveringen ska skötas.![Bild 2](media/2.jpeg)

**Det finns inget i ABK09 som reglerar vem som äger databasen, vad upphovsrätten är värd och vad som gäller för nyttjanderätt och ansvar för den digitala informationen.**

”Avtal för digitala leveranser 2010” har arbetats fram av en arbetsgrupp med representanter för byggherrar, konsulter, byggmaterialindustri och entreprenörer. Arbetet har kontinuerligt stämts av mot en branschrepresentativ referensgrupp. Avtalsmallen är med andra ord väl förankrad i branschen.

AVTALET OMFATTAR ALLT SOM MAN KAN komma överens om när det gäller ett uppdrag om husbyggnad. Diskussioner pågår om att arbeta fram en motsvarande mall för anläggningsprojekt.
​	– I alla fall av husbyggnadsprojektering och där man från början uttrycker att man även vill ha de filer ur vilka ritningarna framställs, så bör detta avtal användas. Det åvilar främst byggherren att specificera den digitala leveransen men även konsulten har ett ansvar att se till att ett avtal upprättas, säger Håkan Blom.